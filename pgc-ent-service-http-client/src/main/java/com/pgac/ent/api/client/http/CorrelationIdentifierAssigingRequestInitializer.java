package com.pgac.ent.api.client.http;

import java.io.IOException;

import com.google.common.base.Preconditions;
import com.pgac.ent.lib.model.service.correlationid.CorrelationIdentifiers;

/**
 * Initializer that transfers our defined correlation identifiers to the request.
 *
 */
public class CorrelationIdentifierAssigingRequestInitializer implements HttpRequestInitializer {
	private final CorrelationIdentifiers identifiers;

	public CorrelationIdentifierAssigingRequestInitializer(CorrelationIdentifiers identifiers) {
		this.identifiers = Preconditions.checkNotNull(identifiers);
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequestInitializer#initialize(com.pgac.ent.api.client.http.HttpRequest)
	 */
	@Override
	public void initialize(HttpRequest request) throws IOException {
		Preconditions.checkNotNull(request);
		request.getHeaders().setClientId(identifiers.getClientId().getValue());
		request.getHeaders().setConversationId(identifiers.getConversationId().getValue());
		request.getHeaders().setUnitId(identifiers.getUnitId().getValue());
	}

	@Override
	public String toString() {
		return "CorrelationIdentifierAssigingRequestInitializer [identifiers=" + identifiers + "]";
	}

}
