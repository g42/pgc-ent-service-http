/*
 * Copyright (c) 2010 Google Inc. Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in
 * writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.pgac.ent.api.client.http;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pgac.ent.api.client.serialization.ObjectParser;
import com.pgac.ent.api.client.util.Beta;
import com.pgac.ent.api.client.util.IOUtils;
import com.pgac.ent.api.client.util.LoggingStreamingContent;
import com.pgac.ent.api.client.util.Preconditions;
import com.pgac.ent.api.client.util.Sleeper;
import com.pgac.ent.api.client.util.StreamingContent;
import com.pgac.ent.api.client.util.StringUtils;

/**
 * HTTP request.
 * <p>
 * Implementation is not thread-safe.
 * </p>
 *
 * @since 1.0
 * @author Yaniv Inbar
 */
public final class DefaultHttpRequest implements HttpRequest {
	private static final Logger log = Logger.getLogger(DefaultHttpRequest.class.getName());
	/**
	 * HTTP request execute interceptor to intercept the start of {@link #execute()} (before executing the HTTP request) or {@code null} for none.
	 */
	private HttpExecuteInterceptor executeInterceptor;

	/** HTTP request requestHeaders. */
	private HttpHeaders requestHeaders = new HttpHeaders();

	/**
	 * HTTP response requestHeaders.
	 * <p>
	 * For example, this can be used if you want to use a subclass of {@link HttpHeaders} called MyHeaders to process the response:
	 * </p>
	 *
	 * <pre>
	 *
	 * static String executeAndGetValueOfSomeCustomHeader(DefaultHttpRequest request) {
	 * 	MyHeaders responseHeaders = new MyHeaders();
	 * 	request.responseHeaders = responseHeaders;
	 * 	DefaultHttpResponse response = request.execute();
	 * 	return responseHeaders.someCustomHeader;
	 * }
	 * </pre>
	 */
	private HttpHeaders responseHeaders = new HttpHeaders();

	/**
	 * The number of retries that will be allowed to execute before the request will be terminated or {@code 0} to not retry requests. Retries occur
	 * as a result of either {@link HttpUnsuccessfulResponseHandler} or {@link HttpIOExceptionHandler} which handles abnormal HTTP response or the I/O
	 * exception.
	 */
	private int numRetries = 10;

	/**
	 * Determines the limit to the content size that will be logged during {@link #execute()}.
	 * <p>
	 * Content will only be logged if {@link #isLoggingEnabled} is {@code true}.
	 * </p>
	 * <p>
	 * If the content size is greater than this limit then it will not be logged.
	 * </p>
	 * <p>
	 * Can be set to {@code 0} to disable content logging. This is useful for example if content has sensitive data such as authentication
	 * information.
	 * </p>
	 * <p>
	 * Defaults to 16KB.
	 * </p>
	 */
	private int contentLoggingLimit = 0x4000;

	/** Determines whether logging should be enabled for this request. Defaults to {@code true}. */
	private boolean loggingEnabled = true;

	/** Determines whether logging in form of curl commands should be enabled for this request. */
	private boolean curlLoggingEnabled = true;

	/** HTTP request content or {@code null} for none. */
	private HttpContent content;

	/** HTTP transport. */
	private final HttpTransport transport;

	/** HTTP request method or {@code null} for none. */
	private String requestMethod;

	/** HTTP request URL. */
	private GenericUrl url;

	/** Timeout in milliseconds to establish a connection or {@code 0} for an infinite timeout. */
	private int connectTimeout = 20 * 1000;

	/**
	 * Timeout in milliseconds to read data from an established connection or {@code 0} for an infinite timeout.
	 */
	private int readTimeout = 20 * 1000;

	/** HTTP unsuccessful (non-2XX) response handler or {@code null} for none. */
	private HttpUnsuccessfulResponseHandler unsuccessfulResponseHandler;

	/** HTTP I/O exception handler or {@code null} for none. */
	@Beta
	private HttpIOExceptionHandler ioExceptionHandler;

	/** HTTP response interceptor or {@code null} for none. */
	private HttpResponseInterceptor responseInterceptor;

	/** Parser used to parse responses. */
	private ObjectParser objectParser;

	/** HTTP content encoding or {@code null} for none. */
	private HttpEncoding encoding;

	/**
	 * The {@link BackOffPolicy} to use between retry attempts or {@code null} for none.
	 */
	@Deprecated
	@Beta
	private BackOffPolicy backOffPolicy;

	/** Whether to automatically follow redirects ({@code true} by default). */
	private boolean followRedirects = true;

	/**
	 * Whether to throw an exception at the end of {@link #execute()} on an HTTP error code (non-2XX) after all retries and response handlers have
	 * been exhausted ({@code true} by default).
	 */
	private boolean throwExceptionOnExecuteError = true;

	/**
	 * Whether to retry the request if an {@link IOException} is encountered in {@link LowLevelHttpRequest#execute()}.
	 */
	@Deprecated
	@Beta
	private final boolean retryOnExecuteIOException = false;

	/**
	 * Whether to not add the suffix {@link #USER_AGENT_SUFFIX} to the User-Agent header.
	 * <p>
	 * It is {@code false} by default.
	 * </p>
	 */
	private boolean suppressUserAgentSuffix;

	/** Sleeper. */
	private Sleeper sleeper = Sleeper.DEFAULT;

	/**
	 * @param transport
	 *            HTTP transport
	 * @param requestMethod
	 *            HTTP request method or {@code null} for none
	 */
	DefaultHttpRequest(HttpTransport transport, String requestMethod) {
		this.transport = transport;
		setRequestMethod(requestMethod);
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getTransport()
	 */
	@Override
	public HttpTransport getTransport() {
		return transport;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getRequestMethod()
	 */
	@Override
	public String getRequestMethod() {
		return requestMethod;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setRequestMethod(java.lang.String)
	 */
	@Override
	public HttpRequest setRequestMethod(String requestMethod) {
		Preconditions.checkArgument(requestMethod == null || HttpMediaType.matchesToken(requestMethod));
		this.requestMethod = requestMethod;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getUrl()
	 */
	@Override
	public GenericUrl getUrl() {
		return url;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setUrl(com.pgac.ent.api.client.http.GenericUrl)
	 */
	@Override
	public HttpRequest setUrl(GenericUrl url) {
		this.url = Preconditions.checkNotNull(url);
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getContent()
	 */
	@Override
	public HttpContent getContent() {
		return content;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setContent(com.pgac.ent.api.client.http.HttpContent)
	 */
	@Override
	public HttpRequest setContent(HttpContent content) {
		this.content = content;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getEncoding()
	 */
	@Override
	public HttpEncoding getEncoding() {
		return encoding;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setEncoding(com.pgac.ent.api.client.http.HttpEncoding)
	 */
	@Override
	public HttpRequest setEncoding(HttpEncoding encoding) {
		this.encoding = encoding;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getContentLoggingLimit()
	 */
	@Override
	public int getContentLoggingLimit() {
		return contentLoggingLimit;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setContentLoggingLimit(int)
	 */
	@Override
	public HttpRequest setContentLoggingLimit(int contentLoggingLimit) {
		Preconditions.checkArgument(contentLoggingLimit >= 0, "The content logging limit must be non-negative.");
		this.contentLoggingLimit = contentLoggingLimit;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#isLoggingEnabled()
	 */
	@Override
	public boolean isLoggingEnabled() {
		return loggingEnabled;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setLoggingEnabled(boolean)
	 */
	@Override
	public HttpRequest setLoggingEnabled(boolean loggingEnabled) {
		this.loggingEnabled = loggingEnabled;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#isCurlLoggingEnabled()
	 */
	@Override
	public boolean isCurlLoggingEnabled() {
		return curlLoggingEnabled;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setCurlLoggingEnabled(boolean)
	 */
	@Override
	public HttpRequest setCurlLoggingEnabled(boolean curlLoggingEnabled) {
		this.curlLoggingEnabled = curlLoggingEnabled;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getConnectTimeout()
	 */
	@Override
	public int getConnectTimeout() {
		return connectTimeout;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setConnectTimeout(int)
	 */
	@Override
	public HttpRequest setConnectTimeout(int connectTimeout) {
		Preconditions.checkArgument(connectTimeout >= 0);
		this.connectTimeout = connectTimeout;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getReadTimeout()
	 */
	@Override
	public int getReadTimeout() {
		return readTimeout;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setReadTimeout(int)
	 */
	@Override
	public HttpRequest setReadTimeout(int readTimeout) {
		Preconditions.checkArgument(readTimeout >= 0);
		this.readTimeout = readTimeout;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getHeaders()
	 */
	@Override
	public HttpHeaders getHeaders() {
		return requestHeaders;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setHeaders(com.pgac.ent.api.client.http.HttpHeaders)
	 */
	@Override
	public HttpRequest setHeaders(HttpHeaders headers) {
		this.requestHeaders = Preconditions.checkNotNull(headers);
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getResponseHeaders()
	 */
	@Override
	public HttpHeaders getResponseHeaders() {
		return responseHeaders;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setResponseHeaders(com.pgac.ent.api.client.http.HttpHeaders)
	 */
	@Override
	public HttpRequest setResponseHeaders(HttpHeaders responseHeaders) {
		this.responseHeaders = Preconditions.checkNotNull(responseHeaders);
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getInterceptor()
	 */
	@Override
	public HttpExecuteInterceptor getInterceptor() {
		return executeInterceptor;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setInterceptor(com.pgac.ent.api.client.http.HttpExecuteInterceptor)
	 */
	@Override
	public HttpRequest setInterceptor(HttpExecuteInterceptor interceptor) {
		this.executeInterceptor = interceptor;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getUnsuccessfulResponseHandler()
	 */
	@Override
	public HttpUnsuccessfulResponseHandler getUnsuccessfulResponseHandler() {
		return unsuccessfulResponseHandler;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setUnsuccessfulResponseHandler(com.pgac.ent.api.client.http.HttpUnsuccessfulResponseHandler)
	 */
	@Override
	public HttpRequest setUnsuccessfulResponseHandler(HttpUnsuccessfulResponseHandler unsuccessfulResponseHandler) {
		this.unsuccessfulResponseHandler = unsuccessfulResponseHandler;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getIOExceptionHandler()
	 */
	@Override
	@Beta
	public HttpIOExceptionHandler getIOExceptionHandler() {
		return ioExceptionHandler;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setIOExceptionHandler(com.pgac.ent.api.client.http.HttpIOExceptionHandler)
	 */
	@Override
	@Beta
	public HttpRequest setIOExceptionHandler(HttpIOExceptionHandler ioExceptionHandler) {
		this.ioExceptionHandler = ioExceptionHandler;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getResponseInterceptor()
	 */
	@Override
	public HttpResponseInterceptor getResponseInterceptor() {
		return responseInterceptor;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setResponseInterceptor(com.pgac.ent.api.client.http.HttpResponseInterceptor)
	 */
	@Override
	public HttpRequest setResponseInterceptor(HttpResponseInterceptor responseInterceptor) {
		this.responseInterceptor = responseInterceptor;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getNumberOfRetries()
	 */
	@Override
	public int getNumberOfRetries() {
		return numRetries;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setNumberOfRetries(int)
	 */
	@Override
	public HttpRequest setNumberOfRetries(int numRetries) {
		Preconditions.checkArgument(numRetries >= 0);
		this.numRetries = numRetries;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setParser(com.pgac.ent.api.client.serialization.ObjectParser)
	 */
	@Override
	public HttpRequest setParser(ObjectParser parser) {
		this.objectParser = parser;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getParser()
	 */
	@Override
	public final ObjectParser getParser() {
		return objectParser;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getFollowRedirects()
	 */
	@Override
	public boolean getFollowRedirects() {
		return followRedirects;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setFollowRedirects(boolean)
	 */
	@Override
	public HttpRequest setFollowRedirects(boolean followRedirects) {
		this.followRedirects = followRedirects;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getThrowExceptionOnExecuteError()
	 */
	@Override
	public boolean getThrowExceptionOnExecuteError() {
		return throwExceptionOnExecuteError;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setThrowExceptionOnExecuteError(boolean)
	 */
	@Override
	public HttpRequest setThrowExceptionOnExecuteError(boolean throwExceptionOnExecuteError) {
		this.throwExceptionOnExecuteError = throwExceptionOnExecuteError;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getSuppressUserAgentSuffix()
	 */
	@Override
	public boolean getSuppressUserAgentSuffix() {
		return suppressUserAgentSuffix;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setSuppressUserAgentSuffix(boolean)
	 */
	@Override
	public HttpRequest setSuppressUserAgentSuffix(boolean suppressUserAgentSuffix) {
		this.suppressUserAgentSuffix = suppressUserAgentSuffix;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#execute()
	 */
	@Override
	public DefaultHttpResponse execute() throws IOException {

		boolean retryRequest = false;
		Preconditions.checkArgument(numRetries >= 0);
		int retriesRemaining = numRetries;
		if (backOffPolicy != null) {
			// Reset the BackOffPolicy at the start of each execute.
			backOffPolicy.reset();
		}
		DefaultHttpResponse response = null;
		IOException executeException;

		Preconditions.checkNotNull(requestMethod);
		Preconditions.checkNotNull(url);

		do {
			// Cleanup any unneeded response from a previous iteration
			if (response != null) {
				response.ignore();
			}

			response = null;
			executeException = null;

			// run the interceptor
			if (executeInterceptor != null) {
				executeInterceptor.intercept(this);
			}
			// build low-level HTTP request
			final String urlString = url.build();
			final LowLevelHttpRequest lowLevelHttpRequest = transport.buildRequest(requestMethod, urlString);
			final Logger logger = HttpTransport.LOGGER;
			final boolean loggable = loggingEnabled && logger.isLoggable(Level.CONFIG);
			StringBuilder logbuf = null;
			StringBuilder curlbuf = null;
			// log method and URL
			if (loggable) {
				logbuf = new StringBuilder();
				logbuf.append("-------------- REQUEST  --------------").append(StringUtils.LINE_SEPARATOR);
				logbuf.append(requestMethod).append(' ').append(urlString).append(StringUtils.LINE_SEPARATOR);

				// setup curl logging
				if (curlLoggingEnabled) {
					curlbuf = new StringBuilder("curl -v --compressed");
					if (!requestMethod.equals(HttpMethods.GET)) {
						curlbuf.append(" -X ").append(requestMethod);
					}
				}
			}
			// add to user agent
			final String originalUserAgent = requestHeaders.getUserAgent();
			if (!suppressUserAgentSuffix) {
				if (originalUserAgent == null) {
					requestHeaders.setUserAgent(USER_AGENT_SUFFIX);
				} else {
					requestHeaders.setUserAgent(originalUserAgent + " " + USER_AGENT_SUFFIX);
				}
			}
			// requestHeaders
			HttpHeaders.serializeHeaders(requestHeaders, logbuf, curlbuf, logger, lowLevelHttpRequest);
			if (!suppressUserAgentSuffix) {
				// set the original user agent back so that retries do not keep appending to it
				requestHeaders.setUserAgent(originalUserAgent);
			}

			// content
			StreamingContent streamingContent = content;
			final boolean contentRetrySupported = streamingContent == null || content.retrySupported();
			if (streamingContent != null) {
				final String contentEncoding;
				final long contentLength;
				final String contentType = content.getType();
				// log content
				if (loggable) {
					streamingContent = new LoggingStreamingContent(streamingContent, HttpTransport.LOGGER, Level.CONFIG, contentLoggingLimit);
				}
				// encoding
				if (encoding == null) {
					contentEncoding = null;
					contentLength = content.getLength();
				} else {
					contentEncoding = encoding.getName();
					streamingContent = new HttpEncodingStreamingContent(streamingContent, encoding);
					contentLength = contentRetrySupported ? IOUtils.computeLength(streamingContent) : -1;
				}
				// append content requestHeaders to log buffer
				if (loggable) {
					if (contentType != null) {
						final String header = "Content-Type: " + contentType;
						logbuf.append(header).append(StringUtils.LINE_SEPARATOR);
						if (curlbuf != null) {
							curlbuf.append(" -H '" + header + "'");
						}
					}
					if (contentEncoding != null) {
						final String header = "Content-Encoding: " + contentEncoding;
						logbuf.append(header).append(StringUtils.LINE_SEPARATOR);
						if (curlbuf != null) {
							curlbuf.append(" -H '" + header + "'");
						}
					}
					if (contentLength >= 0) {
						final String header = "Content-Length: " + contentLength;
						logbuf.append(header).append(StringUtils.LINE_SEPARATOR);
						// do not log @ curl as the user will most likely manipulate the content
					}
				}
				if (curlbuf != null) {
					curlbuf.append(" -d '@-'");
				}
				// send content information to low-level HTTP request
				lowLevelHttpRequest.setContentType(contentType);
				lowLevelHttpRequest.setContentEncoding(contentEncoding);
				lowLevelHttpRequest.setContentLength(contentLength);
				lowLevelHttpRequest.setStreamingContent(streamingContent);
			}
			// log from buffer
			if (loggable) {
				logger.config(logbuf.toString());
				if (curlbuf != null) {
					curlbuf.append(" -- '");
					curlbuf.append(urlString.replaceAll("\'", "'\"'\"'"));
					curlbuf.append("'");
					if (streamingContent != null) {
						curlbuf.append(" << $$$");
					}
					logger.config(curlbuf.toString());
				}
			}

			// We need to make sure our content type can support retry
			// null content is inherently able to be retried
			retryRequest = contentRetrySupported && retriesRemaining > 0;

			// execute
			lowLevelHttpRequest.setTimeout(connectTimeout, readTimeout);
			try {
				final LowLevelHttpResponse lowLevelHttpResponse = lowLevelHttpRequest.execute();
				// Flag used to indicate if an exception is thrown before the response is constructed.
				boolean responseConstructed = false;
				try {
					response = new DefaultHttpResponse(this, lowLevelHttpResponse);
					responseConstructed = true;
				} finally {
					if (!responseConstructed) {
						final InputStream lowLevelContent = lowLevelHttpResponse.getContent();
						if (lowLevelContent != null) {
							lowLevelContent.close();
						}
					}
				}
			} catch (final IOException e) {
				if (!retryOnExecuteIOException && (ioExceptionHandler == null || !ioExceptionHandler.handleIOException(this, retryRequest))) {
					throw e;
				}
				// Save the exception in case the retries do not work and we need to re-throw it later.
				executeException = e;
				logger.log(Level.WARNING, "exception thrown while executing request", e);
			}

			// Flag used to indicate if an exception is thrown before the response has completed
			// processing.
			boolean responseProcessed = false;
			try {
				if (response != null && !response.isSuccessStatusCode()) {
					boolean errorHandled = false;
					if (unsuccessfulResponseHandler != null) {
						// Even if we don't have the potential to retry, we might want to run the
						// handler to fix conditions (like expired tokens) that might cause us
						// trouble on our next request
						errorHandled = unsuccessfulResponseHandler.handleResponse(this, response, retryRequest);
					}
					if (!errorHandled) {
						if (handleRedirect(response.getStatusCode(), response.getHeaders())) {
							// The unsuccessful request's error could not be handled and it is a redirect request.
							errorHandled = true;
						}
					}
					// A retry is required if the error was successfully handled or if it is a redirect
					// request or if the back off policy determined a retry is necessary.
					retryRequest &= errorHandled;
					// need to close the response stream before retrying a request
					if (retryRequest) {
						response.ignore();
					}
				} else {
					// Retry is not required for a successful status code unless the response is null.
					retryRequest &= response == null;
				}
				// Once there are no more retries remaining, this will be -1
				// Count redirects as retries, we want a finite limit of redirects.
				retriesRemaining--;

				responseProcessed = true;
			} finally {
				if (response != null && !responseProcessed) {
					response.disconnect();
				}
			}
		} while (retryRequest);

		if (response == null) {
			// Retries did not help resolve the execute exception, re-throw it.
			throw executeException;
		}
		// response interceptor
		if (responseInterceptor != null) {
			responseInterceptor.interceptResponse(response);
		}
		// throw an exception if unsuccessful response
		if (throwExceptionOnExecuteError && !response.isSuccessStatusCode()) {
			try {
				throw new HttpResponseException(response);
			} finally {
				response.disconnect();
			}
		}
		return response;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#executeAsync(java.util.concurrent.Executor)
	 */
	@Override
	@Beta
	public Future<DefaultHttpResponse> executeAsync(Executor executor) {
		final FutureTask<DefaultHttpResponse> future = new FutureTask<DefaultHttpResponse>(new Callable<DefaultHttpResponse>() {

			@Override
			public DefaultHttpResponse call() throws Exception {
				return execute();
			}
		});
		executor.execute(future);
		return future;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#executeAsync()
	 */
	@Override
	@Beta
	public Future<DefaultHttpResponse> executeAsync() {
		return executeAsync(Executors.newSingleThreadExecutor());
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#handleRedirect(int, com.pgac.ent.api.client.http.HttpHeaders)
	 */
	@Override
	public boolean handleRedirect(int statusCode, HttpHeaders responseHeaders) {
		final String redirectLocation = responseHeaders.getLocation();
		HttpStatusCode httpStatus = HttpStatusCode.get(statusCode);
		if (getFollowRedirects() && httpStatus.isRedirect() && redirectLocation != null) {
			// resolve the redirect location relative to the current location
			setUrl(new GenericUrl(url.toURL(redirectLocation)));
			// on 303 change method to GET
			if (HttpStatusCode.SEE_OTHER == httpStatus) {
				setRequestMethod(HttpMethods.GET);
				// GET requests do not support non-zero content length
				setContent(null);
			}
			// remove Authorization and If-* requestHeaders
			requestHeaders.setAuthorization((String) null);
			requestHeaders.setIfMatch((String) null);
			requestHeaders.setIfNoneMatch((String) null);
			requestHeaders.setIfModifiedSince((String) null);
			requestHeaders.setIfUnmodifiedSince((String) null);
			requestHeaders.setIfRange((String) null);
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#getSleeper()
	 */
	@Override
	public Sleeper getSleeper() {
		return sleeper;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpRequest#setSleeper(com.pgac.ent.api.client.util.Sleeper)
	 */
	@Override
	public HttpRequest setSleeper(Sleeper sleeper) {
		this.sleeper = Preconditions.checkNotNull(sleeper);
		return this;
	}

	@Override
	public String toString() {
		return "DefaultHttpRequest [executeInterceptor=" + executeInterceptor + ", requestHeaders=" + requestHeaders + ", responseHeaders=" + responseHeaders
				+ ", numRetries=" + numRetries + ", contentLoggingLimit=" + contentLoggingLimit + ", loggingEnabled=" + loggingEnabled
				+ ", curlLoggingEnabled=" + curlLoggingEnabled + ", content=" + content + ", transport=" + transport + ", requestMethod="
				+ requestMethod + ", url=" + url + ", connectTimeout=" + connectTimeout + ", readTimeout=" + readTimeout
				+ ", unsuccessfulResponseHandler=" + unsuccessfulResponseHandler + ", ioExceptionHandler=" + ioExceptionHandler
				+ ", responseInterceptor=" + responseInterceptor + ", objectParser=" + objectParser + ", encoding=" + encoding + ", followRedirects="
				+ followRedirects + ", throwExceptionOnExecuteError=" + throwExceptionOnExecuteError + ", suppressUserAgentSuffix="
				+ suppressUserAgentSuffix + ", sleeper=" + sleeper + "]";
	}
}
