/*
 * Copyright (c) 2010 Google Inc. Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in
 * writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.pgac.ent.api.client.http;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

import com.pgac.ent.api.client.util.Charsets;
import com.pgac.ent.api.client.util.IOUtils;
import com.pgac.ent.api.client.util.LoggingInputStream;
import com.pgac.ent.api.client.util.Preconditions;
import com.pgac.ent.api.client.util.StringUtils;
import com.pgac.ent.lib.model.service.DefaultServiceResponse;
import com.pgac.ent.lib.model.service.ExceptionalResponseException;
import com.pgac.ent.lib.model.service.ServiceResponse;
import com.pgac.ent.lib.model.service.status.StatusCode;

/**
 * HTTP response.
 * <p>
 * Callers should call {@link #disconnect} when the HTTP response object is no longer needed. However, {@link #disconnect} does not have to be called
 * if the response stream is properly closed. Example usage:
 * </p>
 *
 * <pre>
 * DefaultHttpResponse response = request.execute();
 * try {
 * 	// process the HTTP response object
 * } finally {
 * 	response.disconnect();
 * }
 * </pre>
 * <p>
 * Implementation is not thread-safe.
 * </p>
 *
 * @since 1.0
 * @author Yaniv Inbar
 */
public final class DefaultHttpResponse implements HttpResponse {

	/** HTTP response content or {@code null} before {@link #getContent()}. */
	private InputStream content;

	/** Content encoding or {@code null}. */
	private final String contentEncoding;

	/** Content type or {@code null} for none. */
	private final String contentType;

	/** Parsed content-type/media type or {@code null} if content-type is null. */
	private final HttpMediaType mediaType;

	/** Low-level HTTP response. */
	LowLevelHttpResponse response;

	/** Status code. */
	private final int statusCode;

	/** Status message or {@code null}. */
	private final String statusMessage;

	/** HTTP request. */
	private final HttpRequest request;

	/**
	 * Determines the limit to the content size that will be logged during {@link #getContent()}.
	 * <p>
	 * Content will only be logged if {@link #isLoggingEnabled} is {@code true}.
	 * </p>
	 * <p>
	 * If the content size is greater than this limit then it will not be logged.
	 * </p>
	 * <p>
	 * Can be set to {@code 0} to disable content logging. This is useful for example if content has sensitive data such as authentication
	 * information.
	 * </p>
	 * <p>
	 * Defaults to {@link HttpRequest#getContentLoggingLimit()}.
	 * </p>
	 */
	private int contentLoggingLimit;

	/**
	 * Determines whether logging should be enabled on this response.
	 * <p>
	 * Defaults to {@link HttpRequest#isLoggingEnabled()}.
	 * </p>
	 */
	private boolean loggingEnabled;

	/** Signals whether the content has been read from the input stream. */
	private boolean contentRead;

	DefaultHttpResponse(HttpRequest request, LowLevelHttpResponse response) throws IOException {
		this.request = request;
		contentLoggingLimit = request.getContentLoggingLimit();
		loggingEnabled = request.isLoggingEnabled();
		this.response = response;
		contentEncoding = response.getContentEncoding();
		final int code = response.getStatusCode();
		statusCode = code < 0 ? 0 : code;
		final String message = response.getReasonPhrase();
		statusMessage = message;
		final Logger logger = HttpTransport.LOGGER;
		final boolean loggable = loggingEnabled && logger.isLoggable(Level.CONFIG);
		StringBuilder logbuf = null;
		if (loggable) {
			logbuf = new StringBuilder();
			logbuf.append("-------------- RESPONSE --------------").append(StringUtils.LINE_SEPARATOR);
			final String statusLine = response.getStatusLine();
			if (statusLine != null) {
				logbuf.append(statusLine);
			} else {
				logbuf.append(statusCode);
				if (message != null) {
					logbuf.append(' ').append(message);
				}
			}
			logbuf.append(StringUtils.LINE_SEPARATOR);
		}

		// headers
		request.getResponseHeaders().fromHttpResponse(response, loggable ? logbuf : null);

		// Retrieve the content-type directly from the headers as response.getContentType() is outdated
		// and e.g. not set by BatchUnparsedResponse.FakeLowLevelHttpResponse
		String contentType = response.getContentType();
		if (contentType == null) {
			contentType = request.getResponseHeaders().getContentType();
		}
		this.contentType = contentType;
		mediaType = contentType == null ? null : new HttpMediaType(contentType);

		// log from buffer
		if (loggable) {
			logger.config(logbuf.toString());
		}
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getContentLoggingLimit()
	 */
	@Override
	public int getContentLoggingLimit() {
		return contentLoggingLimit;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#setContentLoggingLimit(int)
	 */
	@Override
	public HttpResponse setContentLoggingLimit(int contentLoggingLimit) {
		Preconditions.checkArgument(contentLoggingLimit >= 0, "The content logging limit must be non-negative.");
		this.contentLoggingLimit = contentLoggingLimit;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#isLoggingEnabled()
	 */
	@Override
	public boolean isLoggingEnabled() {
		return loggingEnabled;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#setLoggingEnabled(boolean)
	 */
	@Override
	public HttpResponse setLoggingEnabled(boolean loggingEnabled) {
		this.loggingEnabled = loggingEnabled;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getContentEncoding()
	 */
	@Override
	public String getContentEncoding() {
		return contentEncoding;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getContentType()
	 */
	@Override
	public String getContentType() {
		return contentType;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getMediaType()
	 */
	@Override
	public HttpMediaType getMediaType() {
		return mediaType;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getHeaders()
	 */
	@Override
	public HttpHeaders getHeaders() {
		return request.getResponseHeaders();
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#isSuccessStatusCode()
	 */
	@Override
	public boolean isSuccessStatusCode() {
		return HttpStatusCode.get(statusCode).isSuccess();
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getStatusCode()
	 */
	@Override
	public int getStatusCode() {
		return statusCode;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getStatusMessage()
	 */
	@Override
	public String getStatusMessage() {
		return statusMessage;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getTransport()
	 */
	@Override
	public HttpTransport getTransport() {
		return request.getTransport();
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getRequest()
	 */
	@Override
	public HttpRequest getRequest() {
		return request;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getContent()
	 */
	@Override
	public InputStream getContent() throws IOException {
		if (!contentRead) {
			InputStream lowLevelResponseContent = response.getContent();
			if (lowLevelResponseContent != null) {
				// Flag used to indicate if an exception is thrown before the content is successfully
				// processed.
				boolean contentProcessed = false;
				try {
					// gzip encoding (wrap content with GZipInputStream)
					final String contentEncoding = this.contentEncoding;
					if ((contentEncoding != null) && contentEncoding.contains("gzip")) {
						lowLevelResponseContent = new GZIPInputStream(lowLevelResponseContent);
					}
					// logging (wrap content with LoggingInputStream)
					final Logger logger = HttpTransport.LOGGER;
					if (loggingEnabled && logger.isLoggable(Level.CONFIG)) {
						lowLevelResponseContent = new LoggingInputStream(lowLevelResponseContent, logger, Level.CONFIG, contentLoggingLimit);
					}
					content = lowLevelResponseContent;
					contentProcessed = true;
				} catch (final EOFException e) {
					// this may happen for example on a HEAD request since there no actual response data read
					// in GZIPInputStream
				} finally {
					if (!contentProcessed) {
						lowLevelResponseContent.close();
					}
				}
			}
			contentRead = true;
		}
		return content;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#download(java.io.OutputStream)
	 */
	@Override
	public void download(OutputStream outputStream) throws IOException {
		final InputStream inputStream = getContent();
		IOUtils.copy(inputStream, outputStream);
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#ignore()
	 */
	@Override
	public void ignore() throws IOException {
		final InputStream content = getContent();
		if (content != null) {
			content.close();
		}
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#disconnect()
	 */
	@Override
	public void disconnect() throws IOException {
		ignore();
		response.disconnect();
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#parseAs(java.lang.Class)
	 */
	@Override
	public <T> T parseAs(Class<T> dataClass) throws IOException {
		if (!hasMessageBody()) {
			return null;
		}
		return request.getParser().parseAndClose(getContent(), getContentCharset(), dataClass);
	}

	@Override
	public <T, S extends Enum<S> & StatusCode<S>> ServiceResponse<T, S> parseAsServiceResponse(Class<T> dataClass) throws IOException {
		final DefaultServiceResponse.Builder<T, S> serviceResponseBuilder = DefaultServiceResponse.builder();
		if (isSuccessStatusCode()) {
			T t = request.getParser().parseAndClose(getContent(), getContentCharset(), dataClass);
			serviceResponseBuilder.payload(t);
		} else {
			ExceptionalResponseException esr = request.getParser().parseAndClose(getContent(), getContentCharset(),
					ExceptionalResponseException.class);
			serviceResponseBuilder.exception(esr);
		}
		return serviceResponseBuilder.get();
	}

	/**
	 * Returns whether this response contains a message body as specified in {@href http://tools.ietf.org/html/rfc2616#section-4.3}, calling
	 * {@link #ignore()} if {@code false}.
	 */
	private boolean hasMessageBody() throws IOException {
		HttpStatusCode status = HttpStatusCode.get(getStatusCode());
		if (getRequest().getRequestMethod().equals(HttpMethods.HEAD) || status.hasMessageBody()) {
			ignore();
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#parseAs(java.lang.reflect.Type)
	 */
	@Override
	public Object parseAs(Type dataType) throws IOException {
		if (!hasMessageBody()) {
			return null;
		}
		return request.getParser().parseAndClose(getContent(), getContentCharset(), dataType);
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#parseAsString()
	 */
	@Override
	public String parseAsString() throws IOException {
		final InputStream content = getContent();
		if (content == null) {
			return "";
		}
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(content, out);
		return out.toString(getContentCharset().name());
	}

	/* (non-Javadoc)
	 * @see com.pgac.ent.api.client.http.HttpResponse#getContentCharset()
	 */
	@Override
	public Charset getContentCharset() {
		return (mediaType == null) || (mediaType.getCharsetParameter() == null) ? Charsets.ISO_8859_1 : mediaType.getCharsetParameter();
	}

	@Override
	public String toString() {
		return "DefaultHttpResponse [contentEncoding=" + contentEncoding + ", contentType=" + contentType + ", mediaType=" + mediaType
				+ ", statusCode=" + statusCode + ", statusMessage=" + statusMessage + ", request=" + request + ", contentLoggingLimit="
				+ contentLoggingLimit + ", loggingEnabled=" + loggingEnabled + ", contentRead=" + contentRead + "]";
	}

}
