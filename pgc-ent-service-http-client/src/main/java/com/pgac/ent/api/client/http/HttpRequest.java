package com.pgac.ent.api.client.http;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;

import com.pgac.ent.api.client.serialization.ObjectParser;
import com.pgac.ent.api.client.util.Beta;
import com.pgac.ent.api.client.util.Sleeper;

/**
 * Base interface to model an HTTP request. This is not to be a truly "general purpose"
 * HTTP request; design choices will be made to tailor its use specifically to PGC's microservice
 * communication pipeline. In the original codebase, this was a concrete class.
 */
public interface HttpRequest {

	/**
	 * User agent prefix for all requests.
	 */
	String USER_AGENT_PREFIX = "PGC-HTTP-Java-Client";

	/**
	 * Current version of the Google API Client Library for Java.
	 *
	 * @since 1.8
	 */
	//TODO: Investigate generation step to insert this according to project config
	String VERSION = "0.1.0-SNAPSHOT";

	/**
	 * User agent suffix for all requests.
	 * <p>
	 * Includes a {@code "(gzip)"} suffix in case the server -- as Google's servers may do -- checks the {@code User-Agent} header to try to detect if
	 * the client accepts gzip-encoded responses.
	 * </p>
	 *
	 * @since 1.4
	 */
	String USER_AGENT_SUFFIX = USER_AGENT_PREFIX + "/" + VERSION + " (gzip)";

	/**
	 * Returns the HTTP transport.
	 *
	 * @since 1.5
	 */
	HttpTransport getTransport();

	/**
	 * Returns the HTTP request method or {@code null} for none.
	 *
	 * @since 1.12
	 */
	String getRequestMethod();

	/**
	 * Sets the HTTP request method or {@code null} for none.
	 *
	 * @since 1.12
	 */
	HttpRequest setRequestMethod(String requestMethod);

	/**
	 * Returns the HTTP request URL.
	 *
	 * @since 1.5
	 */
	GenericUrl getUrl();

	/**
	 * Sets the HTTP request URL.
	 *
	 * @since 1.5
	 */
	HttpRequest setUrl(GenericUrl url);

	/**
	 * Returns the HTTP request content or {@code null} for none.
	 *
	 * @since 1.5
	 */
	HttpContent getContent();

	/**
	 * Sets the HTTP request content or {@code null} for none.
	 *
	 * @since 1.5
	 */
	HttpRequest setContent(HttpContent content);

	/**
	 * Returns the HTTP content encoding or {@code null} for none.
	 *
	 * @since 1.14
	 */
	HttpEncoding getEncoding();

	/**
	 * Sets the HTTP content encoding or {@code null} for none.
	 *
	 * @since 1.14
	 */
	HttpRequest setEncoding(HttpEncoding encoding);

	/**
	 * Returns the limit to the content size that will be logged during {@link #execute()}.
	 * <p>
	 * If the content size is greater than this limit then it will not be logged.
	 * </p>
	 * <p>
	 * Content will only be logged if {@link #isLoggingEnabled} is {@code true}.
	 * </p>
	 * <p>
	 * Can be set to {@code 0} to disable content logging. This is useful for example if content has sensitive data such as authentication
	 * information.
	 * </p>
	 * <p>
	 * Defaults to 16KB.
	 * </p>
	 *
	 * @since 1.7
	 */
	int getContentLoggingLimit();

	/**
	 * Set the limit to the content size that will be logged during {@link #execute()}.
	 * <p>
	 * If the content size is greater than this limit then it will not be logged.
	 * </p>
	 * <p>
	 * Content will only be logged if {@link #isLoggingEnabled} is {@code true}.
	 * </p>
	 * <p>
	 * Can be set to {@code 0} to disable content logging. This is useful for example if content has sensitive data such as authentication
	 * information.
	 * </p>
	 * <p>
	 * Defaults to 16KB.
	 * </p>
	 *
	 * @since 1.7
	 */
	HttpRequest setContentLoggingLimit(int contentLoggingLimit);

	/**
	 * Returns whether logging should be enabled for this request.
	 * <p>
	 * Defaults to {@code true}.
	 * </p>
	 *
	 * @since 1.9
	 */
	boolean isLoggingEnabled();

	/**
	 * Sets whether logging should be enabled for this request.
	 * <p>
	 * Defaults to {@code true}.
	 * </p>
	 *
	 * @since 1.9
	 */
	HttpRequest setLoggingEnabled(boolean loggingEnabled);

	/**
	 * Returns whether logging in form of curl commands is enabled for this request.
	 *
	 * @since 1.11
	 */
	boolean isCurlLoggingEnabled();

	/**
	 * Sets whether logging in form of curl commands should be enabled for this request.
	 * <p>
	 * Defaults to {@code true}.
	 * </p>
	 *
	 * @since 1.11
	 */
	HttpRequest setCurlLoggingEnabled(boolean curlLoggingEnabled);

	/**
	 * Returns the timeout in milliseconds to establish a connection or {@code 0} for an infinite timeout.
	 *
	 * @since 1.5
	 */
	int getConnectTimeout();

	/**
	 * Sets the timeout in milliseconds to establish a connection or {@code 0} for an infinite timeout.
	 * <p>
	 * By default it is 20000 (20 seconds).
	 * </p>
	 *
	 * @since 1.5
	 */
	HttpRequest setConnectTimeout(int connectTimeout);

	/**
	 * Returns the timeout in milliseconds to read data from an established connection or {@code 0} for an infinite timeout.
	 * <p>
	 * By default it is 20000 (20 seconds).
	 * </p>
	 *
	 * @since 1.5
	 */
	int getReadTimeout();

	/**
	 * Sets the timeout in milliseconds to read data from an established connection or {@code 0} for an infinite timeout.
	 *
	 * @since 1.5
	 */
	HttpRequest setReadTimeout(int readTimeout);

	/**
	 * Returns the HTTP request headers.
	 *
	 * @since 1.5
	 */
	HttpHeaders getHeaders();

	/**
	 * Sets the HTTP request headers.
	 * <p>
	 * By default, this is a new unmodified instance of {@link HttpHeaders}.
	 * </p>
	 *
	 * @since 1.5
	 */
	HttpRequest setHeaders(HttpHeaders headers);

	/**
	 * Returns the HTTP response headers.
	 *
	 * @since 1.5
	 */
	HttpHeaders getResponseHeaders();

	/**
	 * Sets the HTTP response headers.
	 * <p>
	 * By default, this is a new unmodified instance of {@link HttpHeaders}.
	 * </p>
	 * <p>
	 * For example, this can be used if you want to use a subclass of {@link HttpHeaders} called MyHeaders to process the response:
	 * </p>
	 *
	 * <pre>
	 *
	 * static String executeAndGetValueOfSomeCustomHeader(DefaultHttpRequest request) {
	 * 	MyHeaders responseHeaders = new MyHeaders();
	 * 	request.responseHeaders = responseHeaders;
	 * 	DefaultHttpResponse response = request.execute();
	 * 	return responseHeaders.someCustomHeader;
	 * }
	 * </pre>
	 *
	 * @since 1.5
	 */
	HttpRequest setResponseHeaders(HttpHeaders responseHeaders);

	/**
	 * Returns the HTTP request execute interceptor to intercept the start of {@link #execute()} (before executing the HTTP request) or {@code null}
	 * for none.
	 *
	 * @since 1.5
	 */
	HttpExecuteInterceptor getInterceptor();

	/**
	 * Sets the HTTP request execute interceptor to intercept the start of {@link #execute()} (before executing the HTTP request) or {@code null} for
	 * none.
	 *
	 * @since 1.5
	 */
	HttpRequest setInterceptor(HttpExecuteInterceptor interceptor);

	/**
	 * Returns the HTTP unsuccessful (non-2XX) response handler or {@code null} for none.
	 *
	 * @since 1.5
	 */
	HttpUnsuccessfulResponseHandler getUnsuccessfulResponseHandler();

	/**
	 * Sets the HTTP unsuccessful (non-2XX) response handler or {@code null} for none.
	 *
	 * @since 1.5
	 */
	HttpRequest setUnsuccessfulResponseHandler(HttpUnsuccessfulResponseHandler unsuccessfulResponseHandler);

	/**
	 * {@link Beta} <br/>
	 * Returns the HTTP I/O exception handler or {@code null} for none.
	 *
	 * @since 1.15
	 */
	HttpIOExceptionHandler getIOExceptionHandler();

	/**
	 * {@link Beta} <br/>
	 * Sets the HTTP I/O exception handler or {@code null} for none.
	 *
	 * @since 1.15
	 */
	HttpRequest setIOExceptionHandler(HttpIOExceptionHandler ioExceptionHandler);

	/**
	 * Returns the HTTP response interceptor or {@code null} for none.
	 *
	 * @since 1.13
	 */
	HttpResponseInterceptor getResponseInterceptor();

	/**
	 * Sets the HTTP response interceptor or {@code null} for none.
	 *
	 * @since 1.13
	 */
	HttpRequest setResponseInterceptor(HttpResponseInterceptor responseInterceptor);

	/**
	 * Returns the number of retries that will be allowed to execute before the request will be terminated or {@code 0} to not retry requests. Retries
	 * occur as a result of either {@link HttpUnsuccessfulResponseHandler} or {@link HttpIOExceptionHandler} which handles abnormal HTTP response or
	 * the I/O exception.
	 *
	 * @since 1.5
	 */
	int getNumberOfRetries();

	/**
	 * Sets the number of retries that will be allowed to execute before the request will be terminated or {@code 0} to not retry requests. Retries
	 * occur as a result of either {@link HttpUnsuccessfulResponseHandler} or {@link HttpIOExceptionHandler} which handles abnormal HTTP response or
	 * the I/O exception.
	 * <p>
	 * The default value is {@code 10}.
	 * </p>
	 *
	 * @since 1.5
	 */
	HttpRequest setNumberOfRetries(int numRetries);

	/**
	 * Sets the {@link ObjectParser} used to parse the response to this request or {@code null} for none.
	 * <p>
	 * This parser will be preferred over any registered HttpParser.
	 * </p>
	 *
	 * @since 1.10
	 */
	HttpRequest setParser(ObjectParser parser);

	/**
	 * Returns the {@link ObjectParser} used to parse the response or {@code null} for none.
	 *
	 * @since 1.10
	 */
	ObjectParser getParser();

	/**
	 * Returns whether to follow redirects automatically.
	 *
	 * @since 1.6
	 */
	boolean getFollowRedirects();

	/**
	 * Sets whether to follow redirects automatically.
	 * <p>
	 * The default value is {@code true}.
	 * </p>
	 *
	 * @since 1.6
	 */
	HttpRequest setFollowRedirects(boolean followRedirects);

	/**
	 * Returns whether to throw an exception at the end of {@link #execute()} on an HTTP error code (non-2XX) after all retries and response handlers
	 * have been exhausted.
	 *
	 * @since 1.7
	 */
	boolean getThrowExceptionOnExecuteError();

	/**
	 * Sets whether to throw an exception at the end of {@link #execute()} on a HTTP error code (non-2XX) after all retries and response handlers have
	 * been exhausted.
	 * <p>
	 * The default value is {@code true}.
	 * </p>
	 *
	 * @since 1.7
	 */
	HttpRequest setThrowExceptionOnExecuteError(boolean throwExceptionOnExecuteError);

	/**
	 * Returns whether to not add the suffix {@link #USER_AGENT_SUFFIX} to the User-Agent header.
	 *
	 * @since 1.11
	 */
	boolean getSuppressUserAgentSuffix();

	/**
	 * Sets whether to not add the suffix {@link #USER_AGENT_SUFFIX} to the User-Agent header.
	 * <p>
	 * The default value is {@code false}.
	 * </p>
	 *
	 * @since 1.11
	 */
	HttpRequest setSuppressUserAgentSuffix(boolean suppressUserAgentSuffix);

	/**
	 * Execute the HTTP request and returns the HTTP response.
	 * <p>
	 * Note that regardless of the returned status code, the HTTP response content has not been parsed yet, and must be parsed by the calling code.
	 * </p>
	 * <p>
	 * Note that when calling to this method twice or more, the state of this HTTP request object isn't cleared, so the request will continue where it
	 * was left. For example, the state of the {@link HttpUnsuccessfulResponseHandler} attached to this HTTP request will remain the same as it was
	 * left after last execute.
	 * </p>
	 * <p>
	 * Almost all details of the request and response are logged if {@link Level#CONFIG} is loggable. The only exception is the value of the
	 * {@code Authorization} header which is only logged if {@link Level#ALL} is loggable.
	 * </p>
	 * <p>
	 * Callers should call {@link HttpResponse#disconnect} when the returned HTTP response object is no longer needed. However,
	 * {@link HttpResponse#disconnect} does not have to be called if the response stream is properly closed. Example usage:
	 * </p>
	 *
	 * <pre>
	 * DefaultHttpResponse response = request.execute();
	 * try {
	 * 	// process the HTTP response object
	 * } finally {
	 * 	response.disconnect();
	 * }
	 * </pre>
	 *
	 * @return HTTP response for an HTTP success response (or HTTP error response if {@link #getThrowExceptionOnExecuteError()} is {@code false})
	 * @throws HttpResponseException
	 *             for an HTTP error response (only if {@link #getThrowExceptionOnExecuteError()} is {@code true})
	 * @see HttpResponse#isSuccessStatusCode()
	 */
	HttpResponse execute() throws IOException;

	/**
	 * {@link Beta} <br/>
	 * Executes this request asynchronously in a single separate thread using the supplied executor.
	 *
	 * @param executor
	 *            executor to run the asynchronous request
	 * @return future for accessing the HTTP response
	 * @since 1.13
	 */
	Future<DefaultHttpResponse> executeAsync(Executor executor);

	/**
	 * {@link Beta} <br/>
	 * Executes this request asynchronously using {@link #executeAsync(Executor)} in a single separate thread using
	 * {@link Executors#newSingleThreadExecutor()}.
	 *
	 * @return A future for accessing the results of the asynchronous request.
	 * @since 1.13
	 */
	Future<DefaultHttpResponse> executeAsync();

	/**
	 * Sets up this request object to handle the necessary redirect if redirects are turned on, it is a redirect status code and the header has a
	 * location.
	 * <p>
	 * When the status code is {@code 303} the method on the request is changed to a GET as per the RFC2616 specification. On a redirect, it also
	 * removes the {@code "Authorization"} and all {@code "If-*"} request headers.
	 * </p>
	 * <p>
	 * Upgrade warning: When handling a status code of 303, {@link #handleRedirect(int, HttpHeaders)} now correctly removes any content from the body
	 * of the new request, as GET requests should not have content. It did not do this in prior version 1.16.
	 * </p>
	 *
	 * @return whether the redirect was successful
	 * @since 1.11
	 */
	boolean handleRedirect(int statusCode, HttpHeaders responseHeaders);

	/**
	 * Returns the sleeper.
	 *
	 * @since 1.15
	 */
	Sleeper getSleeper();

	/**
	 * Sets the sleeper. The default value is {@link Sleeper#DEFAULT}.
	 *
	 * @since 1.15
	 */
	HttpRequest setSleeper(Sleeper sleeper);

}