/*
 * Copyright (c) 2011 Google Inc. Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in
 * writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.pgac.ent.api.client.http;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.Validate;

import com.google.common.collect.ImmutableList;
import com.pgac.ent.api.client.util.Preconditions;
import com.pgac.ent.lib.model.service.correlationid.CorrelationIdentifiers;
import com.pgac.ent.lib.model.service.http.RequestIdentifiers;

/**
 * Thread-safe light-weight HTTP request factory layer on top of the HTTP transport that has  {@link HttpRequestInitializer} HTTP request
 * initializer instances for initializing requests.
 * <p>
 * For example, to use a particular authorization header across all requests, use:
 * </p>
 *
 * <pre>
 *
 * public static HttpRequestFactory createRequestFactory(HttpTransport transport) {
 * 	return transport.createRequestFactory(new HttpRequestInitializer() {
 *
 * 		public void initialize(DefaultHttpRequest request) throws IOException {
 * 			request.getHeaders().setAuthorization("...");
 * 		}
 * 	});
 * }
 * </pre>
 * <p>
 * This implemention will put a defaulting-behavior {@link CorrelationIdentifierAssigingRequestInitializer} into
 * the stack of {@link HttpRequestInitializer} instances provided during creation time; this is less-than-useful
 * as the identifiers will naturally be defaulted to an easily-spottable value codified by {@link CorrelationIdentifiers#getUnspecified()}.
 *
 * @since 1.4
 * @author Yaniv Inbar
 */
public final class HttpRequestFactory {
	static final HttpRequestInitializer DEFAULT_CORRELATION_INITIALIZER = new CorrelationIdentifierAssigingRequestInitializer(
			new RequestIdentifiers(null, null, null));
	/** HTTP transport. */
	private final HttpTransport transport;

	/** HTTP Request initializerList **/
	private final List<HttpRequestInitializer> initializerList;

	private boolean correlationAssigningInitializerSpecified;

	/**
	 * @param transport
	 *            HTTP transport
	 * @param initializer
	 *            HTTP request initializerList or {@code null} for none
	 */
	HttpRequestFactory(HttpTransport transport, HttpRequestInitializer... initializers) {
		this.transport = Preconditions.checkNotNull(transport);
		this.initializerList = new ArrayList<>();
		if (initializers != null) {
			for (HttpRequestInitializer i : initializers) {
				initializerList.add(Validate.notNull(i));
				if (ClassUtils.isAssignable(CorrelationIdentifierAssigingRequestInitializer.class, i.getClass())) {
					correlationAssigningInitializerSpecified = true;
				}
			}
		}
		if (!correlationAssigningInitializerSpecified) {
			initializerList.add(DEFAULT_CORRELATION_INITIALIZER);
		}
	}

	/**
	 * Constructs an {@link HttpRequestFactory} with no initializerList.
	 * @param transport
	 *            HTTP transport
	 */
	HttpRequestFactory(HttpTransport transport) {
		this(transport, (HttpRequestInitializer[]) null);

	}

	/**
	 * Returns the HTTP transport.
	 *
	 * @since 1.5
	 */
	public HttpTransport getTransport() {
		return transport;
	}

	/**
	 * Returns the HTTP request initializer or {@code null} for none.
	 * <p>
	 * This initializer is invoked before setting its method, URL, or content.
	 * </p>
	 *
	 * @since 1.5
	 */
	public List<HttpRequestInitializer> getInitializers() {
		return ImmutableList.copyOf(initializerList);
	}

	/**
	 * Builds a request for the given HTTP method, URL, and content.
	 *
	 * @param requestMethod
	 *            HTTP request method
	 * @param url
	 *            HTTP request URL or {@code null} for none
	 * @param content
	 *            HTTP request content or {@code null} for none
	 * @return new HTTP request
	 * @since 1.12
	 */
	public HttpRequest buildRequest(String requestMethod, GenericUrl url, HttpContent content) throws IOException {
		final HttpRequest request = transport.buildRequest();
		if (!initializerList.isEmpty()) {
			for (HttpRequestInitializer i : initializerList) {
				i.initialize(request);
			}
		}
		request.setRequestMethod(requestMethod);
		if (url != null) {
			request.setUrl(url);
		}
		if (content != null) {
			request.setContent(content);
		}
		return request;
	}

	/**
	 * Builds a {@code DELETE} request for the given URL.
	 *
	 * @param url
	 *            HTTP request URL or {@code null} for none
	 * @return new HTTP request
	 */
	public HttpRequest buildDeleteRequest(GenericUrl url) throws IOException {
		return buildRequest(HttpMethods.DELETE, url, null);
	}

	/**
	 * Builds a {@code GET} request for the given URL.
	 *
	 * @param url
	 *            HTTP request URL or {@code null} for none
	 * @return new HTTP request
	 */
	public HttpRequest buildGetRequest(GenericUrl url) throws IOException {
		return buildRequest(HttpMethods.GET, url, null);
	}

	/**
	 * Builds a {@code POST} request for the given URL and content.
	 *
	 * @param url
	 *            HTTP request URL or {@code null} for none
	 * @param content
	 *            HTTP request content or {@code null} for none
	 * @return new HTTP request
	 */
	public HttpRequest buildPostRequest(GenericUrl url, HttpContent content) throws IOException {
		return buildRequest(HttpMethods.POST, url, content);
	}

	/**
	 * Builds a {@code PUT} request for the given URL and content.
	 *
	 * @param url
	 *            HTTP request URL or {@code null} for none
	 * @param content
	 *            HTTP request content or {@code null} for none
	 * @return new HTTP request
	 */
	public HttpRequest buildPutRequest(GenericUrl url, HttpContent content) throws IOException {
		return buildRequest(HttpMethods.PUT, url, content);
	}

	/**
	 * Builds a {@code PATCH} request for the given URL and content.
	 *
	 * @param url
	 *            HTTP request URL or {@code null} for none
	 * @param content
	 *            HTTP request content or {@code null} for none
	 * @return new HTTP request
	 */
	public HttpRequest buildPatchRequest(GenericUrl url, HttpContent content) throws IOException {
		return buildRequest(HttpMethods.PATCH, url, content);
	}

	/**
	 * Builds a {@code HEAD} request for the given URL.
	 *
	 * @param url
	 *            HTTP request URL or {@code null} for none
	 * @return new HTTP request
	 */
	public HttpRequest buildHeadRequest(GenericUrl url) throws IOException {
		return buildRequest(HttpMethods.HEAD, url, null);
	}
}
