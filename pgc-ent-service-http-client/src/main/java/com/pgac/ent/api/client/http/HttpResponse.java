package com.pgac.ent.api.client.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

import com.pgac.ent.lib.model.service.ServiceResponse;
import com.pgac.ent.lib.model.service.status.StatusCode;

/**
 * Base interface to model an HTTP response. This is not to be a truly "general purpose"
 * HTTP response; design choices will be made to tailor its use specifically to PGC's microservice
 * communication pipeline.
 *
 */
public interface HttpResponse {

	/**
	 * Returns the limit to the content size that will be logged during {@link #getContent()}.
	 * <p>
	 * Content will only be logged if {@link #isLoggingEnabled} is {@code true}.
	 * </p>
	 * <p>
	 * If the content size is greater than this limit then it will not be logged.
	 * </p>
	 * <p>
	 * Can be set to {@code 0} to disable content logging. This is useful for example if content has sensitive data such as authentication
	 * information.
	 * </p>
	 * <p>
	 * Defaults to {@link HttpRequest#getContentLoggingLimit()}.
	 * </p>
	 *
	 * @since 1.7
	 */
	int getContentLoggingLimit();

	/**
	 * Set the limit to the content size that will be logged during {@link #getContent()}.
	 * <p>
	 * Content will only be logged if {@link #isLoggingEnabled} is {@code true}.
	 * </p>
	 * <p>
	 * If the content size is greater than this limit then it will not be logged.
	 * </p>
	 * <p>
	 * Can be set to {@code 0} to disable content logging. This is useful for example if content has sensitive data such as authentication
	 * information.
	 * </p>
	 * <p>
	 * Defaults to {@link HttpRequest#getContentLoggingLimit()}.
	 * </p>
	 *
	 * @since 1.7
	 */
	HttpResponse setContentLoggingLimit(int contentLoggingLimit);

	/**
	 * Returns whether logging should be enabled on this response.
	 * <p>
	 * Defaults to {@link HttpRequest#isLoggingEnabled()}.
	 * </p>
	 *
	 * @since 1.9
	 */
	boolean isLoggingEnabled();

	/**
	 * Sets whether logging should be enabled on this response.
	 * <p>
	 * Defaults to {@link HttpRequest#isLoggingEnabled()}.
	 * </p>
	 *
	 * @since 1.9
	 */
	HttpResponse setLoggingEnabled(boolean loggingEnabled);

	/**
	 * Returns the content encoding or {@code null} for none.
	 *
	 * @since 1.5
	 */
	String getContentEncoding();

	/**
	 * Returns the content type or {@code null} for none.
	 *
	 * @since 1.5
	 */
	String getContentType();

	/**
	 * Returns the parsed Content-Type in form of a {@link HttpMediaType} or {@code null} if no content-type was set.
	 *
	 * @since 1.10
	 */
	HttpMediaType getMediaType();

	/**
	 * Returns the HTTP response headers.
	 *
	 * @since 1.5
	 */
	HttpHeaders getHeaders();

	/**
	 * Returns whether received a successful HTTP status code {@code >= 200 && < 300} (see {@link #getStatusCode()}).
	 *
	 * @since 1.5
	 */
	boolean isSuccessStatusCode();

	/**
	 * Returns the HTTP status code or {@code 0} for none.
	 *
	 * @since 1.5
	 */
	int getStatusCode();

	/**
	 * Returns the HTTP status message or {@code null} for none.
	 *
	 * @since 1.5
	 */
	String getStatusMessage();

	/**
	 * Returns the HTTP transport.
	 *
	 * @since 1.5
	 */
	HttpTransport getTransport();

	/**
	 * Returns the HTTP request.
	 *
	 * @since 1.5
	 */
	HttpRequest getRequest();

	/**
	 * Returns the content of the HTTP response.
	 * <p>
	 * The result is cached, so subsequent calls will be fast.
	 * <p>
	 * Callers should call {@link InputStream#close} after the returned {@link InputStream} is no longer needed. Example usage:
	 *
	 * <pre>
	 * InputStream is = response.getContent();
	 * try {
	 * 	// Process the input stream..
	 * } finally {
	 * 	is.close();
	 * }
	 * </pre>
	 * <p>
	 * {@link HttpResponse#disconnect} does not have to be called if the content is closed.
	 *
	 * @return input stream content of the HTTP response or {@code null} for none
	 * @throws IOException
	 *             I/O exception
	 */
	InputStream getContent() throws IOException;

	/**
	 * Writes the content of the HTTP response into the given destination output stream.
	 * <p>
	 * Sample usage:
	 *
	 * <pre>
	 * DefaultHttpRequest request = requestFactory.buildGetRequest(new GenericUrl("https://www.google.com/images/srpr/logo3w.png"));
	 * OutputStream outputStream = new FileOutputStream(new File("/tmp/logo3w.png"));
	 * try {
	 * 	DefaultHttpResponse response = request.execute();
	 * 	response.download(outputStream);
	 * } finally {
	 * 	outputStream.close();
	 * }
	 * </pre>
	 * </p>
	 * <p>
	 * This method closes the content of the HTTP response from {@link #getContent()}.
	 * </p>
	 * <p>
	 * This method does not close the given output stream.
	 * </p>
	 *
	 * @param outputStream
	 *            destination output stream
	 * @throws IOException
	 *             I/O exception
	 * @since 1.9
	 */
	void download(OutputStream outputStream) throws IOException;

	/**
	 * Closes the content of the HTTP response from {@link #getContent()}, ignoring any content.
	 */
	void ignore() throws IOException;

	/**
	 * Close the HTTP response content using {@link #ignore}, and disconnect using {@link LowLevelHttpResponse#disconnect()}.
	 *
	 * @since 1.4
	 */
	void disconnect() throws IOException;

	/**
	 * Parses the content of the HTTP response from {@link #getContent()} and reads it into a data class of key/value pairs using the parser returned
	 * by {@link HttpRequest#getParser()}.
	 * <p>
	 * <b>Reference:</b> http://tools.ietf.org/html/rfc2616#section-4.3
	 * </p>
	 *
	 * @return parsed data class or {@code null} for no content
	 */
	<T> T parseAs(Class<T> dataClass) throws IOException;

	<T, S extends Enum<S> & StatusCode<S>> ServiceResponse<T, S> parseAsServiceResponse(Class<T> dataClass) throws IOException;

	/**
	 * Parses the content of the HTTP response from {@link #getContent()} and reads it into a data type of key/value pairs using the parser returned
	 * by {@link HttpRequest#getParser()}.
	 *
	 * @return parsed data type instance or {@code null} for no content
	 * @since 1.10
	 */
	Object parseAs(Type dataType) throws IOException;

	/**
	 * Parses the content of the HTTP response from {@link #getContent()} and reads it into a string.
	 * <p>
	 * Since this method returns {@code ""} for no content, a simpler check for no content is to check if {@link #getContent()} is {@code null}.
	 * </p>
	 * <p>
	 * All content is read from the input content stream rather than being limited by the Content-Length. For the character set, it follows the
	 * specification by parsing the "charset" parameter of the Content-Type header or by default {@code "ISO-8859-1"} if the parameter is missing.
	 * </p>
	 *
	 * @return parsed string or {@code ""} for no content
	 * @throws IOException
	 *             I/O exception
	 */
	String parseAsString() throws IOException;

	/**
	 * Returns the {@link Charset} specified in the Content-Type of this response or the {@code "ISO-8859-1"} charset as a default.
	 *
	 * @since 1.10
	 */
	Charset getContentCharset();

}
