package com.pgac.ent.api.client.http;

import java.util.HashMap;
import java.util.Map;

import com.pgac.ent.lib.model.service.status.StatusCode;
import com.pgac.ent.lib.model.service.status.StatusCodeClassification;

public enum HttpStatusCode implements StatusCode<HttpStatusCode> {
	//@formatter:off
	CONTINUE(100, "Continue"),
	SWITCHING_PROTOCOLS(101, "Swtiching Protocols"),
	PROCESSING(102, "Processing"),
    OK(200, "OK"),
    CREATED(201, "Created"),
    ACCEPTED(202, "Accepted"),
    NO_CONTENT(204, "No Content"),
    RESET_CONTENT(205, "Reset Content"),
    PARTIAL_CONTENT(206, "Partial Content"),
    MOVED_PERMANENTLY(301, "Moved Permanently"),
    FOUND(302, "Found"),
    SEE_OTHER(303, "See Other"),
    NOT_MODIFIED(304, "Not Modified"),
    USE_PROXY(305, "Use Proxy"),
    TEMPORARY_REDIRECT(307, "Temporary Redirect"),
    BAD_REQUEST(400, "Bad Request"),
    UNAUTHORIZED(401, "Unauthorized"),
    PAYMENT_REQUIRED(402, "Payment Required"),
    FORBIDDEN(403, "Forbidden"),
    NOT_FOUND(404, "Not Found"),
    METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
    NOT_ACCEPTABLE(406, "Not Acceptable"),
    PROXY_AUTHENTICATION_REQUIRED(407, "Proxy Authentication Required"),
    REQUEST_TIMEOUT(408, "Request Timeout"),
    CONFLICT(409, "Conflict"),
    GONE(410, "Gone"),
    LENGTH_REQUIRED(411, "Length Required"),
    PRECONDITION_FAILED(412, "Precondition Failed"),
    REQUEST_ENTITY_TOO_LARGE(413, "Request Entity Too Large"),
    REQUEST_URI_TOO_LONG(414, "Request-URI Too Long"),
    UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type"),
    REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested Range Not Satisfiable"),
    EXPECTATION_FAILED(417, "Expectation Failed"),
    IM_A_TEAPOT(418, "I'm a Teapot"),
    UNPROCESSABLE_ENTITY(422, "Unprocessable Entity"),
    INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
    NOT_IMPLEMENTED(501, "Not Implemented"),
    BAD_GATEWAY(502, "Bad Gateway"),
    SERVICE_UNAVAILABLE(503, "Service Unavailable"),
    GATEWAY_TIMEOUT(504, "Gateway Timeout");
	//@formatter:on

	private int code;
	private String description;
	private static final Map<Integer, HttpStatusCode> LOOKUP_MAP;

	static {
		LOOKUP_MAP = new HashMap<>();
		for (HttpStatusCode c : HttpStatusCode.values()) {
			LOOKUP_MAP.put(c.code, c);
		}
	}

	private HttpStatusCode(int code, String description) {
		this.code = code;
		this.description = description;
	}

	public static HttpStatusCode get(int code) {
		if (LOOKUP_MAP.containsKey(code)) {
			return LOOKUP_MAP.get(code);
		} else {
			//if you can't provide a valid status code, you get the fail
			return INTERNAL_SERVER_ERROR;
		}
	}

	@Override
	public int code() {
		return code;
	}

	@Override
	public String description() {
		return description;
	}

	@Override
	public StatusCodeClassification classification() {
		return StatusCode.classificationOf(code);
	}

	public boolean isRedirect() {
		return StatusCodeClassification.REDIRECTION == classification();
	}

	/**
	 * Uses the HTTP status code to ascertain whether a response should have a message body.
	 * <p>
	 * <em>NOTE:</em>:You cannot fully depend on status code alone to determine this, the HTTP Method
	 * (notably, HEAD) also is part of the detemination.
	 *
	 * @see <a href="http://tools.ietf.org/html/rfc2616#section-4.3">RFC262 section 4.3</a>
	 *
	 * @return true, if the status code indicates (along with HttpMethod.HEAD) if this response has a message body
	 */
	public boolean hasMessageBody() {
		return (StatusCodeClassification.INFORMATIONAL == classification()) || (this == HttpStatusCode.NO_CONTENT)
				|| (this == HttpStatusCode.NOT_MODIFIED);
	}

}
