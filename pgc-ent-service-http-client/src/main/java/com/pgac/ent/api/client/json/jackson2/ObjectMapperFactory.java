package com.pgac.ent.api.client.json.jackson2;

import java.util.List;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.collect.Lists;

/**
 * Jackson ObjectMapperFactory with all locally-required modules registered.
 */
public class ObjectMapperFactory {

	private static final ObjectMapper JSON_MAPPER;

	static {
		JSON_MAPPER = new ObjectMapper();
		for (Module m : modules()) {
			JSON_MAPPER.registerModule(m);
		}
		for (SerializationConfigBundle b : getConfigurations()) {
			JSON_MAPPER.configure(b.feature, b.state);
		}
	}

	private ObjectMapperFactory() {
	}

	private static List<Module> modules() {
		return Lists.newArrayList(new JavaTimeModule(), new Jdk8Module()); //adding support for Java8 time/date, Optional
	}

	private static List<SerializationConfigBundle> getConfigurations() {
		return Lists.newArrayList(new SerializationConfigBundle(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false));
	}

	public static ObjectMapper objectMapper() {
		return JSON_MAPPER;
	}

	private static class SerializationConfigBundle {

		final SerializationFeature feature;
		final boolean state;

		public SerializationConfigBundle(SerializationFeature feature, boolean state) {
			super();
			this.feature = feature;
			this.state = state;
		}
	}

}
