package com.pgac.ent.api.client.http;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.pgac.ent.api.client.http.apache.ApacheHttpTransport;
import com.pgac.ent.api.client.json.JsonFactory;
import com.pgac.ent.api.client.json.JsonObjectParser;
import com.pgac.ent.api.client.json.jackson2.JacksonFactory;
import com.pgac.ent.lib.model.service.ExceptionalResponseException;
import com.pgac.ent.lib.model.service.ServiceResponse;
import com.pgac.ent.lib.model.service.testing.acmeservice.AcmeUser;
import com.pgac.ent.lib.model.service.testing.acmeservice.AcmeUserManager;
import com.pgac.ent.lib.model.service.testing.acmeservice.AcmeUserService;
import com.pgac.ent.lib.model.service.testing.acmeservice.AcmeUserServiceStatusCodes;

/**
 * This class consumes the local embedded "Acme User Service" which codifies
 * expected behaviors (e.g. expecting and returning correlation IDs in headers,
 * returning something parseable as {@link ExceptionalResponseException} on failure,
 * etc.
 */
public class AcmeConsumingHttpResponseTest {

	private static final JsonFactory JSON_FACTORY = new JacksonFactory();
	private static final HttpTransport HTTP_TRANSPORT = new ApacheHttpTransport();
	private static AcmeUserService service;
	private static String rootUri;
	private static HttpRequestFactory requestFactory;

	@BeforeClass
	public static void beforeClass() {
		service = new AcmeUserService();
		service.start();
		rootUri = service.getRootUri();
		requestFactory = HTTP_TRANSPORT.createRequestFactory((requestInitializer) -> {
			requestInitializer.setParser(new JsonObjectParser(JSON_FACTORY));
			requestInitializer.setLoggingEnabled(true);
			requestInitializer.setThrowExceptionOnExecuteError(false);
		});
	}

	@AfterClass
	public static void afterClass() {
		if (service != null) {
			service.stop();
		}
	}

	@Test
	public void nonExistentUserShouldReturnExceptionalResponse() throws IOException {
		String uri = rootUri + AcmeUserManager.TRIGGER_USERNAME_NOT_FOUND;
		HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(uri));
		ServiceResponse<AcmeUser, AcmeUserServiceStatusCodes> dsr = request.execute().parseAsServiceResponse(AcmeUser.class);
		assertNull(dsr.getResult());
		assertThat(dsr.getExceptionalResponse().getApplicationStatusCode(), is(AcmeUserServiceStatusCodes.USER_NOT_FOUND));
	}

	@Test
	public void existingUserShouldReturnInServiceResponse() throws IOException {
		String uri = rootUri + AcmeUserManager.TRIGGER_USERNAME_NORMAL;
		HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(uri));
		ServiceResponse<AcmeUser, AcmeUserServiceStatusCodes> dsr = request.execute().parseAsServiceResponse(AcmeUser.class);
		assertThat(dsr.getResult().getUsername(), is(AcmeUserManager.TRIGGER_USERNAME_NORMAL));
	}
}
