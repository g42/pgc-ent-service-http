package com.pgac.ent.api.client.http;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Test;

import com.pgac.ent.api.client.testing.http.MockHttpTransport;
import com.pgac.ent.lib.model.service.correlationid.CorrelationIdentifiers;
import com.pgac.ent.lib.model.service.correlationid.SequentialIntegerCorrelationIdSupplier;
import com.pgac.ent.lib.model.service.http.RequestIdentifiers;

public class CorrelationIdentifierAssigingRequestInitializerTest {

	@Test
	public void initializerShouldAssignExpectedValues() throws IOException {
		MockHttpTransport transport = new MockHttpTransport();
		SequentialIntegerCorrelationIdSupplier supplier = new SequentialIntegerCorrelationIdSupplier();
		CorrelationIdentifiers identifiers = new RequestIdentifiers(supplier.get(), supplier.get(), supplier.get());
		HttpRequestInitializer initializer = new CorrelationIdentifierAssigingRequestInitializer(identifiers);
		HttpRequestFactory rf = transport.createRequestFactory(initializer);
		HttpHeaders headers = rf.buildGetRequest(new GenericUrl("http://x.com")).getHeaders();
		assertThat(headers.getClientId(), is(identifiers.getClientId().getValue()));
		assertThat(headers.getConversationId(), is(identifiers.getConversationId().getValue()));
		assertThat(headers.getUnitId(), is(identifiers.getUnitId().getValue()));
	}

	@Test
	public void initializerShouldAssignUnspecifiedIdOnNullInput() throws IOException {
		MockHttpTransport transport = new MockHttpTransport();
		CorrelationIdentifiers identifiers = new RequestIdentifiers(null, null, null);
		HttpRequestInitializer initializer = new CorrelationIdentifierAssigingRequestInitializer(identifiers);
		HttpRequestFactory rf = transport.createRequestFactory(initializer);
		HttpHeaders headers = rf.buildGetRequest(new GenericUrl("http://x.com")).getHeaders();
		assertThat(headers.getClientId(), is(identifiers.getUnspecified().getValue()));
		assertThat(headers.getConversationId(), is(identifiers.getUnspecified().getValue()));
		assertThat(headers.getUnitId(), is(identifiers.getUnspecified().getValue()));
	}
}
