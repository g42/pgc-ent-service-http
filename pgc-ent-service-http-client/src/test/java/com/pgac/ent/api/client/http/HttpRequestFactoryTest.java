package com.pgac.ent.api.client.http;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.ClassUtils;
import org.junit.Test;

import com.pgac.ent.api.client.testing.http.MockHttpTransport;

public class HttpRequestFactoryTest {

	@Test(expected = NullPointerException.class)
	public void shouldNotAcceptNullTransport() {
		new HttpRequestFactory(null);
	}

	/**
	 * If no initializers are set, we set a dummy implementation of {@link HttpRequestInitializer}, namely
	 * {@link CorrelationIdentifierAssigingRequestInitializer}. There is some synergy here with {@link CorrelationIdentifierAssigingRequestInitializerTest}.
	 * @throws IOException
	 */
	@Test
	public void testDefaultingBehavior() throws IOException {
		HttpRequestFactory rf = new HttpRequestFactory(new MockHttpTransport());
		List<HttpRequestInitializer> initializers = rf.getInitializers();
		boolean expectedImplFound = false;
		HttpRequestInitializer initializer = null;
		search: for (HttpRequestInitializer i : initializers) {
			if (ClassUtils.isAssignable(CorrelationIdentifierAssigingRequestInitializer.class, i.getClass())) {
				expectedImplFound = true;
				initializer = i;
				break search;
			}
		}
		assertThat(expectedImplFound, is(true));
		assertThat(initializer, is(HttpRequestFactory.DEFAULT_CORRELATION_INITIALIZER));
	}
}
